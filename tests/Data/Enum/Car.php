<?php

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\EcomTest\Data\Enum;

/**
 * Enum for testing.
 */
enum Car
{
    case FIAT;
    case VOLVO;
    case TESLA;
    case KIA;
    case TOYOTA;
}
