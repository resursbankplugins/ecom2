<?php

/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Ecom\Lib\Model\PriceSignage;

/**
 * @codingStandardsIgnoreStart
 */
enum Language: string
{
    case SWEDISH = 'Swedish';
    case FINNISH = 'Finnish';
    case NORWEGIAN = 'Norwegian';
    case DANISH = 'Danish';
}
